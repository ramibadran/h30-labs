import eventBus from '@/utilities/eventBus';
import authService from '../services/auth';
import store from '@/store';
import AuthenticateServices from "@/services/authenticate.js";
// handle user login
export const loginMixins = {
    methods: {
        async login() {
            this.sendingRequest = true;
     
            const res = await authService.login({username: this.loginInfo.username,password: this.loginInfo.password});
          
            if(res && !res.errors) {
                this.setLoggedin(res,true);
            } else {
                this.loginErrorMsg = res.messages;
                this.loginError = true;
                this.sendingRequest = false;
            }
        },
        async getUserInfo() {
            const token = sessionStorage.getItem('token');
  
            const res = await authService.getProfileInfo({token});
            
            if(res && !res.errors) {
                res.data.user = res.data;
                res.data.token = token;
                this.setLoggedin(res);
            } else {
                this.loading = false;
            }
            
        },
        setLoggedin(res, showSnackbar = false) {
            this.$store.dispatch('login',{
                token: res.data.token,
                user: res.data.user
            }).then(()=> {
                if(this.rememberUsername) {
                    localStorage.setItem('username',res.data.user.user_name)
                } else {
                    localStorage.removeItem('username');
                }
                sessionStorage.setItem('token',res.data.token);
                showSnackbar && eventBus.$emit('snackbar', {
                    type: 'success',
                    message: `Welcome back, ${res.data.user.first_name}`,
                });
                this.$router.push({name: 'dashboard'})
                if(this.loading) {
                    this.$nextTick(()=> {
                        setTimeout(()=> {
                            this.loading = false;
                        },200);
                    });
                }
            });
        }
    }
};
export const rolesMixins = {
    methods: {
        hasAccess(canObj) {
            for(var o in canObj) {
                if(canObj[o]) return true;
            }
            return false;
        },
        async getAccessibleAttributes(moduleName) {
            const res = await AuthenticateServices.can({
                permission: moduleName
            });
            return res.data.attributes;
        },
        async filterViewAttributes(moduleName,headers) {
            const res = await AuthenticateServices.can({
                permission: moduleName
            });
            const attributes = res.data.attributes;
            headers = headers.filter(function(header) {
                return header.allwaysVisible || attributes.includes(header.value);
            });
            console.log('>>>>>>',headers)
            return headers;
        }
    },
    computed: {
        accessibleModules() {
            return store.getters.roles.modulesRoles;
        },
        accessibleSubModules() {
            return store.getters.roles.subModules;
        },
    }
};