export const removeDash = (text) => {
    return text.replace(/[-|_]/g,' ');
};
export const formatedDataString = (value) => {
    if (!value) {  
        return '' 
    }
    else if(Array.isArray(value)) {
        value = value.join(' • ').replace(/_/g, ' ')
        value = value.toString()
        return capitalizeWords(value);
    } else {
        value = value.replace(/_/g, ' ')
        return capitalizeWords(value);
    }
}
function capitalizeWords(str) {
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();})
}