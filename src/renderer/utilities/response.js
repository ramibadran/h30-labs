import eventBus from '@/utilities/eventBus';

export const handler = (data) => {
      if(!data.success){
        eventBus.$emit('snackbar', {
          type: 'error',
          message: (data.messages) ? data.messages : "System Error",
        })
        return
      }
      return data
}
