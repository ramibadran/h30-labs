import { app, BrowserWindow, Menu } from 'electron'


import { Router } from 'electron-routes';
import ResponseHandler from './api/responses/responseHandler'


require('update-electron-app')
global.Router = Router
global.ResponseHandler = ResponseHandler


const os = require('os');
const storage = require('electron-json-storage');

storage.setDataPath(os.tmpdir());
require("./api/controllers/OrganizationController.js");

import { template } from './menu.js';


/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\')
}

let mainWindow
const winURL = process.env.NODE_ENV === 'development'
  ? `http://localhost:9080`
  : `file://${__dirname}/index.html`

function createWindow () {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    height: 563,
    useContentSize: true,
    width: 1000,
    webPreferences: {
      nodeIntegration: true
    }
  })
  
  mainWindow.loadURL(winURL)
  mainWindow.maximize()
  const menu = Menu.buildFromTemplate(template)
  Menu.setApplicationMenu(menu)
  mainWindow.on('closed', () => {
    mainWindow = null
  })
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */

/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall()
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates()
})
 */

//Added By Rami Badran for auto-update
 //https://www.electronjs.org/docs/tutorial/updates
// if (process.env.NODE_ENV !== 'development') {

    // check for updates
    
    /*const server = 'http://54.242.88.21/'
    const url = `${server}update/${process.platform}/${app.getVersion()}`
    autoUpdater.setFeedURL({ url })
    
    

    //var app1 = require('app');
    //var os1 = require('os');
    //var autoUpdater = require('electron').autoUpdater;

    //var platform = os1.platform() + '_' + os1.arch();  // usually returns darwin_64
    //var version = app1.getVersion();

    //autoUpdater.setFeedURL('http://54.242.88.21/'+platform+'/'+version);

    setInterval(() => {
      autoUpdater.checkForUpdates()
    }, 60000)
    
    //get user notified 
    autoUpdater.on('update-downloaded', (event, releaseNotes, releaseName) => {
      
      var notif={
        title: 'update-downloaded',
      };
      new Notification(notif).show();
    })

    autoUpdater.on('update-available', (event, releaseNotes, releaseName) => {
      
      var notif={
        title: 'update-available',
        body: "event " +  JSON.stringify(event) + "releaseNotes " + releaseNotes + "releaseName " + releaseName

      };
      new Notification(notif).show();
      autoUpdater.quitAndInstall(true, true);
    })

  
  
    autoUpdater.on('checking-for-update', (event, releaseNotes, releaseName) => {
      
      var notif={
        title: 'checking-for-update',
        timeoutType:"never",
        body: "event " +  JSON.stringify(event) + "releaseNotes " + releaseNotes + "releaseName " + releaseName
      };
      new Notification(notif).show();
    
    })

    autoUpdater.on('before-quit-for-update', (event, releaseNotes, releaseName) => {
      
      var notif={
        title: 'before-quit-for-update',
      };
      new Notification(notif).show();
    
    })
    //handling error on update
    autoUpdater.on('error', (event) => {
      var notif={
        title: 'error',
        timeoutType:"never",
        body: "event " + event 
      };
      new Notification(notif).show();
    })*/
	
	/*const server = 'http://54.242.88.21/'
    const url = `${server}update/${process.platform}/${app.getVersion()}`
    autoUpdater.setFeedURL({ url })*/
const {autoUpdater, dialog } = require('electron')
var platform = os.platform() + '_' + os.arch();  // usually returns darwin_64
var version = app.getVersion();

autoUpdater.setFeedURL('http://54.242.88.21/update/'+platform+'/'+version);
    
    setInterval(() => {
	  autoUpdater.checkForUpdates()
	}, 60000)

	autoUpdater.on('update-downloaded', (event, releaseNotes, releaseName) => {
	  const dialogOpts = {
	    type: 'info',
	    buttons: ['Restart', 'Later'],
	    title: 'Application Update',
	    message: process.platform === 'win32' ? releaseNotes : releaseName,
	    detail: 'A new version has been downloaded. Restart the application to apply the updates.'
	  }
	
	  /*dialog.showMessageBox(dialogOpts).then((returnValue) => {
	    if (returnValue.response === 0) autoUpdater.quitAndInstall()
	  })*/
	  
	  dialog.showMessageBox(null, dialogOpts, (response, checkboxChecked) => {
		    console.log(response);
		    //console.log(checkboxChecked);
		  	if (response === 0) autoUpdater.quitAndInstall()
	  });
	  
	  //autoUpdater.quitAndInstall()
	})
	
	autoUpdater.on('error', message => {
  console.error('There was a problem updating the application')
  console.error(message)
})