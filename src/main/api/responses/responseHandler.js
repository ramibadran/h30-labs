/**
 * responseHandler.js
 *
 * A custom response.
 *
 */

var statusBadRequest = 400;
var statusSuccess = 200;
var statusCreated = 201;
var statusUnauthorized = 401;
var statusUnprocurable = 422;
var statusNotFound = 404;

export default class ResponseHandler {
  constructor(req, res,type='success') {
    this.statusCode = statusSuccess;
    if(type == 'created'){
      this.statusCode = statusCreated;
    }else if(type == 'unauthorized'){
      this.statusCode = statusUnauthorized;
    }
    this.res_success = true;
    this.data = null;
    this.errors = false;
    this.res = res;
    this.req = req;
  }

  success(data) {
    try {
      
      return this.res.json({
        errors: this.errors,
        success: this.res_success,
        status: this.statusCode,
        data: data,
      })
    } catch (error) {
      
    }
  }
  error(messages) {
    this.errors = true
    this.res_success = false
    this.statusCode = statusBadRequest
    try {
      
      return this.res.json({
        errors: this.errors,
        success: this.res_success,
        status: this.statusCode,
        messages: messages,
      })
    } catch (error) {
      
    }
   
  }
}
